package ro.tuc.tp;

import model.Monom;
import model.Operatii;
import model.Polinom;
import org.junit.jupiter.api.Test;
import ro.tuc.tp.IntegrateOpTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class IntegrateOpTest {
    @Test
    public void integrateTest() {
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(2, 1));
        p1.addMonom(new Monom(1, 0));

        Polinom rez = new Polinom();
        rez.addMonom(new Monom(1, 2));
        rez.addMonom(new Monom(1, 1));

        assertTrue(Operatii.integrate(p1).toString().equals(rez.toString()), "The result is incorrect");
    }
}
