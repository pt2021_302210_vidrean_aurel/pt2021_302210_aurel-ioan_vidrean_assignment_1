package ro.tuc.tp;

import model.Monom;
import model.Operatii;
import model.Polinom;
import org.junit.jupiter.api.Test;
import ro.tuc.tp.SubtractOpTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SubtractOpTest {
    @Test
    public void subtractTest() {
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(5, 2));
        p1.addMonom(new Monom(-4, 1));
        p1.addMonom(new Monom(1, 0));

        Polinom p2 = new Polinom();
        p2.addMonom(new Monom(4, 4));
        p2.addMonom(new Monom(2, 2));
        p2.addMonom(new Monom(-5, 1));

        Polinom rez = new Polinom();
        rez.addMonom(new Monom(-4, 4));
        rez.addMonom(new Monom(3, 2));
        rez.addMonom(new Monom(1, 1));
        rez.addMonom(new Monom(1, 0));

        assertTrue(Operatii.subtract(p1, p2).toString().equals(rez.toString()), "The result is incorrect");
    }
}