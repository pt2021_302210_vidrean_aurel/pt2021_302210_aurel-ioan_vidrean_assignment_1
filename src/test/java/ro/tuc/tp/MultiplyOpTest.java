package ro.tuc.tp;

import model.Monom;
import model.Operatii;
import model.Polinom;
import org.junit.jupiter.api.Test;
import ro.tuc.tp.MultiplyOpTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MultiplyOpTest {
    @Test
    public void multiplyTest() {
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(5, 2));
        p1.addMonom(new Monom(1, 0));

        Polinom p2 = new Polinom();
        p2.addMonom(new Monom(2, 2));
        p2.addMonom(new Monom(-5, 1));

        Polinom rez = new Polinom();
        rez.addMonom(new Monom(10, 4));
        rez.addMonom(new Monom(-25, 3));
        rez.addMonom(new Monom(2, 2));
        rez.addMonom(new Monom(-5, 1));

        assertTrue(Operatii.multiplication(p1, p2).toString().equals(rez.toString()), "The result is incorrect");
    }
}